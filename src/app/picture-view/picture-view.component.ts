import {Component, Input, OnInit} from '@angular/core';
import {WebsocketService} from '../services/websocket.service';
import {MessageItem} from '../classes/message-item';
import {NewImage} from '../messages/new-image';
import {environment} from '../../environments/environment';

@Component({
    selector: 'app-picture-view',
    templateUrl: './picture-view.component.html',
    styleUrls: ['./picture-view.component.css']
})
export class PictureViewComponent{
    @Input() messages: MessageItem[];

    constructor(private socketService: WebsocketService) {
    }
}
