import {MessageItem} from "../classes/message-item";
import {GenericMessage} from "./generic-message";

export class NewImage extends GenericMessage {
    public payload: MessageItem[];
}
