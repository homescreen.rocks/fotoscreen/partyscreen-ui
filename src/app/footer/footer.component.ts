import {Component, OnInit} from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';
import faHeart from '@fortawesome/fontawesome-free-solid';
import {WebsocketService} from "../services/websocket.service";
import {MessageItem} from "../classes/message-item";

fontawesome.library.add(faHeart);

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

    messages: MessageItem[] = [];

    constructor(private socketService: WebsocketService) {
    }

    ngOnInit() {
        this.socketService.textMessages().subscribe(value => {
            this.messages.push(value);
            if (this.messages.length > 15) {
                this.messages.splice(0, 1);
            }
        });
        let test = new MessageItem();
        test.name = 'Tilmann';
        test.message = 'ticker message';
        test.date = new Date();
        this.messages.push(test);

        // init a periodically check to remove old messages here:
    }

}