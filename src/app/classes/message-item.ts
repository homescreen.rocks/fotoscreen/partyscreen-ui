export class MessageItem {
    id: string;
    name: string;
    date: Date;
    message: string;
    sourceType: string;
    sourceUrl: string;
    userId: string;
}
