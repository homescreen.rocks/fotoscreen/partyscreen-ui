import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoTextViewComponent } from './info-text-view.component';

describe('InfoTextViewComponent', () => {
  let component: InfoTextViewComponent;
  let fixture: ComponentFixture<InfoTextViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoTextViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoTextViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
