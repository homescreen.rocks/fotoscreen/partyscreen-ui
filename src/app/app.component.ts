import {Component} from '@angular/core';
import {WebsocketService} from "./services/websocket.service";
import {environment} from "../environments/environment";
import {NewImage} from "./messages/new-image";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
    active: string;
    messages = [];

    constructor(private socketService: WebsocketService) {
    }

    ngOnInit(): void {
        this.socketService.getOthers().subscribe(value => {
            this.active = 'info';
        });
        this.socketService.getImages().subscribe(value => {
            if (value.event === 'NewImage') {
                const newImageList = <NewImage>value;
                console.log('Got new Image to display: ' + newImageList.payload[0].id);
                this.messages = newImageList.payload.map(mess => {
                    if (mess.sourceType === 'local' && environment.apiServer.substr(0, 2) !== '//') {
                        mess.sourceUrl = '//' + (environment.apiServer ? environment.apiServer : window.location.host) + mess.sourceUrl;
                    }
                    return mess;
                });
            }
            this.active = 'image';
        });
    }
}
